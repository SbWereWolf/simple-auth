<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAuthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
            $table->id();
            $table->string('code', 30)->unique();
            $table->string('title', 50)->nullable();
            $table->text('description')->nullable();
        });

        DB::beginTransaction();

        DB::insert('insert into role (code,title) values(?,?)',
            ['SPEAKER', 'Speaker']);
        DB::insert('insert into role (code,title) values(?,?)',
            ['RESIDENT', 'Resident']);
        DB::insert('insert into role (code,title) values(?,?)',
            ['ATTENDEE', 'Attendee']);
        DB::insert('insert into role (code,title) values(?,?)',
            ['ADMINISTRATOR', 'Administrator']);
        DB::insert('insert into role (code,title) values(?,?)',
            ['CURATOR', 'Curator']);

        Schema::create('auth', function (Blueprint $table) {
            $table->id();
            $table->string('login')->unique();
            $table->text('password_hash');
        });

        Schema::create('profile', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('auth_id')->unique();
            $table->foreign('auth_id')->references('id')->on('auth');
            $table->string('name');
            $table->string('surname');
            $table->string('zip');
            $table->string('city');
            $table->string('country');
            $table->bigInteger('role_id');
            $table->foreign('role_id')->references('id')->on('role');
        });

        Schema::create('session', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('auth_id');
            $table->foreign('auth_id')->references('id')->on('auth');
            $table->string('token')->unique();
            $table->timestamp('active_at');
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session');
        Schema::dropIfExists('profile');
        Schema::dropIfExists('auth');
        Schema::dropIfExists('role');
    }
}
