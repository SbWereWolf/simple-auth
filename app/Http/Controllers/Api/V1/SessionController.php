<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SessionController extends Controller
{
    public function open(Request $request)
    {

        $login = $request->login;

        DB::beginTransaction();
        $data = DB::select(
            'select id,password_hash from auth where login = ? limit 1',
            [$login]);

        $isSuccess = is_array($data) && count($data) === 1;
        $passwordHash = '';
        $authId = 0;
        if ($isSuccess) {
            $passwordHash = $data[0]->password_hash;
            $authId = $data[0]->id;
        }

        $password = $request->password;
        $isValid = password_verify($password, $passwordHash);

        $isEmpty = false;
        $token = '';
        if ($isValid) {
            session_start();
            $token = session_id();
            $isEmpty = empty($token);
        }
        if ($isValid && $isEmpty) {
            $token = session_create_id();
            session_id($token);
        }

        $code = 400;
        if ($isValid) {
            session_write_close();

            DB::insert('
insert into session (auth_id,token,active_at)
select ?,?,CURRENT_TIMESTAMP
',
                [$authId, $token]);

            DB::commit();
            $code = 201;
        }
        if (!$isValid) {
            DB::rollBack();
        }

        $response = response()->json([
            'success' => $isValid,
            'token' => $token],
            $code);

        return $response;
    }

    public function close(string $token)
    {
        DB::beginTransaction();
        $rowAffected = DB::delete(
            'delete from session where token = ?',
            [$token]);

        $code = 404;
        $isSuccess = $rowAffected === 1;
        if ($isSuccess) {
            session_id($token);
            session_start();
            session_unset();
        }
        if ($isSuccess && ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 60 * 60 * 24,
                $params['path'], $params['domain'],
                $params['secure'], $params['httponly']
            );
        }
        if ($isSuccess) {
            session_destroy();
            $code = 204;
            DB::commit();
        }
        if (!$isSuccess) {
            DB::rollBack();
        }

        $response = response()->json(['success' => $isSuccess,],
            $code);

        return $response;
    }
}
