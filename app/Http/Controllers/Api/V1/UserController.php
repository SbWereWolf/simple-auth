<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $rules = [
            'username' => 'required|between:5,50|unique:auth,login',
            'name' => 'required|max:50',
            'surname' => 'required|max:50',
            'password' => 'required|min:6',
            'zip' => 'required|between:5,20',
            'city' => 'required|max:50',
            'country' => 'required|max:100',
            'role' => 'required|exists:role,code',
            'agree' => 'accepted',
        ];
        $messages = [
            'username.required' => 'Заполните имя пользователя для входа в систему',
            'username.between' => 'Имя пользователя может быть от 5 до 50 символов',
            'username.unique' => 'Такое имя пользователя уже используется',
            'name.required' => 'Имя не может быть пустым',
            'name.max' => 'Имя может состоять максимум из 50-ти символов',
            'surname.required' => 'Фамилия не может быть пустой',
            'surname.max' => 'Фамилия может состоять максимум из 50-ти символов',
            'password.required' => 'Пароль не может быть пустым',
            'password.min' => 'Пароль должен быть минимум из 6-ти символов',
            'zip.required' => 'Укажите почтовый индекс',
            'zip.between' => 'Почтовый индекс  может быть от 5 до 20 символов',
            'city.required' => 'Укажите город',
            'city.max' => 'Город может иметь максимум 50 символов',
            'country.required' => 'Укажите страну',
            'country.max' => 'Страна может иметь максимум 100 символов',
            'role.required' => 'Выберите роль',
            'role.exists' => 'Выбранная роль не существует',
            'agree.accepted' => 'Вы должны согласиться с условиями',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $isValid = $validator->passes();

        $response = response();
        if (!$isValid) {
            $errors = $validator->errors();
            $response = response()->json(
                ['success' => false, 'errors' => $errors],
                422);
        }

        if ($isValid) {
            $username = (string)$request->username;
            $password = (string)$request->password;

            DB::beginTransaction();

            $passwordHash = password_hash($password, PASSWORD_BCRYPT);
            $authId = DB::table('auth')
                ->insertGetId([
                    'login' => $username,
                    'password_hash' => $passwordHash,
                ]);

            $name = (string)$request->name;
            $surname = (string)$request->surname;
            $zip = (string)$request->zip;
            $city = (string)$request->city;
            $country = (string)$request->country;
            $role = (string)$request->role;

            DB::insert('
insert into profile(auth_id,name,surname,zip,city,country,role_id)
select ?,?,?,?,?,?,(select id from role where code = ?)
', [$authId, $name, $surname, $zip, $city, $country, $role]);

            DB::commit();

            $response = response()->json(['success' => true], 201);
        }

        return $response;
    }
}
