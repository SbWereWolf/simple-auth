# Underlying data
Реализуйте API аутенфикации, как если бы вы делали ее для 

[Картинка1](https://s.mail.ru/qC4A/57bEBWW4y), 
[Картинка2](https://s.mail.ru/2RNY/N516ZBgrB), 
[Картинка3](https://s.mail.ru/4RMd/2CNWnLRSJ).

Представим, что на клиенте SPA и ему нужно определять статус 
аутенфикации с помощью API. В качестве фреймворка предпочтителен 
Laravel (но не обязателен), т.к. проект будет реализован на нем.

Формальных требований нет. Ваша задача продумать систему аутенфикации 
с нуля и реализовать ее в отведенное время. Все черновики и 
рассуждения желательно приложить к исходному коду. Критерии оценки: 
"чистота" кода, продуманность архитектуры (насколько возможно).

# Черновики и рассуждения
Много рассуждений не будет.

Из картинок понятно что наше АПИ предоставляет две услуги:
 - Регистрация 
 - Вход
 
Странно предоставлять "вход" без "выхода", 
поэтому к нашему АПИ добавим "выход".

По картинкам, при регистрации указывается роль.

Значит делаем справочник. На который по хорошему надо сделать вызов
АПИ для выдачи значений, но мы обозначим намерение и этого достаточно.

При регистрации так же указывается страна, город и почтовый индекс,
что бы уменьшить количество ошибок ввода и для унификации данных, 
эти поля лучше сделать справочниками, но мы об этом только скажем,
создание этих справочников выходит за рамки бюджета этой задачи.

Рассуждаем далее, есть задача аутентификации пользователя, и есть его
профиль.

Это разная информация, которая используется в разных целях, поэтому
разнесём её по разным таблицам, данные для аутентификации в таблицу
auth данные о пользователе - profile.

Справочник ролей - role.

После аутентификации пользователя, для него открывается сессия, 
используем для этого стандартный механизм PHP для аутентификации на
коленке - это лучшее что можно придумать.

Если пользователь решил закрыть сессию, то удаляем его ПХП сессию.

В сессии храним последнюю активность пользователя - active_at, для
определения времени истечения жизни сессии.  

# О качестве исполнения
Качество на уровне черновика и проверки концепции,
кода с логикой едва 200 строк, конечно можно выделить код валидатора
и код бизнес логики создания пользователя и работы с сессией, но
в рамках задачи на 4 часа это оверинжениринг.

# How To Install
```bash
git clone https://gitlab.com/SbWereWolf/simple-auth.git
cd simple-auth
composer install
php artisan migrate

```
# How To Test

Start Laravel Development Server `php artisan serve`

## Test register new user
```
POST http://127.0.0.1:8000/api/v1/user/register
Content-Type: application/x-www-form-urlencoded

country=Россия&city=Екатеринбург&zip=620142&username=12345&name=1&surname=2&password=123456&role=CURATOR&agree=true
###

HTTP/1.1 201 Created
Host: 127.0.0.1:8000
Date: Sat, 25 Jul 2020 13:20:51 GMT
Connection: close
X-Powered-By: PHP/7.4.5
Cache-Control: no-cache, private
Date: Sat, 25 Jul 2020 13:20:51 GMT
Content-Type: application/json
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 58

{
  "success": true
}
```
## Test log in
```
POST http://127.0.0.1:8000/api/v1/session/open
Content-Type: application/x-www-form-urlencoded

login=12345&password=123456
###

HTTP/1.1 201 Created
Host: 127.0.0.1:8000
Date: Sat, 25 Jul 2020 13:49:46 GMT
Connection: close
X-Powered-By: PHP/7.4.5
Set-Cookie: PHPSESSID=uv02uis1leqvhr7q1e99ov93h6; path=/
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Cache-Control: no-store, no-cache, must-revalidate
Pragma: no-cache
Cache-Control: no-cache, private
Date: Sat, 25 Jul 2020 13:49:46 GMT
Content-Type: application/json
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 59

{
  "success": true,
  "token": "uv02uis1leqvhr7q1e99ov93h6"
}
```

## Test log out
```
DELETE http://127.0.0.1:8000/api/v1/session/close/uv02uis1leqvhr7q1e99ov93h6
###

HTTP/1.1 204 No Content
Host: 127.0.0.1:8000
Date: Sat, 25 Jul 2020 14:09:52 GMT
Connection: close
X-Powered-By: PHP/7.4.5
Set-Cookie: PHPSESSID=uv02uis1leqvhr7q1e99ov93h6; path=/
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Cache-Control: no-store, no-cache, must-revalidate
Pragma: no-cache
Cache-Control: no-cache, private
Date: Sat, 25 Jul 2020 14:09:51 GMT
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 59

<Response body is empty>
```
