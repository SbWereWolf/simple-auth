<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('v1/user/register', 'Api\v1\UserController@create');
Route::post('v1/session/open', 'Api\v1\SessionController@open');
Route::delete('v1/session/close/{token}', 'Api\v1\SessionController@close');
